# squad-health-check
Meteor web &amp; mobile app based on Spotify's Squad Health Check idea.

https://squad-health-check-model-telstra-cloud.f172.telstra-ice-osd.openshiftapps.com/

##July 2018 update
- GitHub Authentication
- Pre-populate of the cards 
- In Progress section
- Disabled going forward without answering the question and appropriate alert message shown to the user
- Updated packages
- Title of the health check shown in the stats page
- Admin View for the admin users in the "telstra-ice-spotify-healthcheck" GitHub org
- Publish function for all users
- Each card on stats page can be viewed with its pros and cons
- Better layout in the stats page
- Published health checks now have the name of the owner and date created as tags
- Tooltip on stats
- Tags on the homepage
