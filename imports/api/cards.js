import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Cards = new Mongo.Collection('cards');

if (Meteor.isServer) {
    // This code only runs on the server
    Meteor.publish('cards', () => {
        return Cards.find();
    });
}

Meteor.methods({
    'cards.getPublic'() {
    	if (Cards.find().count() == 0 ){
	    	var DefaultCards = require('../defaultCards.json');
	    	for (var i = 0; i < DefaultCards.length; i++) {
	    		Meteor.call('cards.insert', DefaultCards[i].checkId, DefaultCards[i].title, DefaultCards[i].pros, DefaultCards[i].cons )
	    		}
    	}    	
        return Cards.find({ private: false }).fetch();
    }, 
    'cards.insert'(checkId, title, pros, cons) {
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        
        check(checkId, String);
        check(title, String);
        check(pros, String);
        check(cons, String);
        
        Cards.insert({
            checkId,
            title,
            pros,
            cons,
            private : false
        });
    },
});
