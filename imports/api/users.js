import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { HTTP } from 'meteor/http';
import { Session } from 'meteor/session'

export const User =  Meteor.users;
const organisation = "skpublisher";
const orgRole = "admin";
orgStatus = false;


if (Meteor.isServer) {
  // console.log("meteor is server");
  Meteor.publish("user", function () {
      return User.find({});
  });

  Meteor.publish("userData", function () {
	    return User.find({_id: this.userId});
	});
}

Meteor.methods({
	'userData.getOrgAuth'() {
		const gitToken = Meteor.call('userData.getAccessToken');
		// console.log(gitToken);
		if(gitToken.length == 0) throw new Meteor.Error('not-authorized');

		// console.log("inside git access_token");
		HTTP.call( 'GET', 'https://api.github.com/user/orgs', {
			params: {"access_token": gitToken[0].services.github.accessToken}
        }, function( error, response ) {
          if ( error ) {
          	// console.log(error);
            return  error;
          } else {
            // console.log("Into response from API");
            for (var i = response.data.length - 1; i >= 0; i--) {
                if (response.data[i].login == organisation){
                  // console.log(response.data);
                  // console.log("Organisation found!");
                  Meteor.call('userData.getRole');
                  orgStatus = true;
                  Session.setPersistent('org', 'true');
                }
            }
            if (!orgStatus) {
              Session.clearPersistent();
              Meteor.logout();
           }}
        });
    },
    'userData.getAccessToken'() {
    	// console.log("Inside userdata.getAccessToken Function");
      return User.find({_id: Meteor.userId()},{fields: {'services.github.accessToken': 1}}).fetch();
    },
    'userData.getRole'() {
      const gitToken = Meteor.call('userData.getAccessToken');
      if(gitToken.length == 0) throw new Meteor.Error('not-authorized');

      const user = Meteor.user();
      const username = user.services.github.username;
      
      HTTP.call( 'GET', 'https://api.github.com/orgs/' + organisation +'/memberships/' + username , {
      params: {"access_token": gitToken[0].services.github.accessToken}
        }, function( error, response ) {
          if ( error ) {
            // console.log(error);
            return  error;
          } else {         
            if (response.data.role == orgRole){
              Session.setPersistent('admin', 'true');
            }
          }
        });
    }
}); 