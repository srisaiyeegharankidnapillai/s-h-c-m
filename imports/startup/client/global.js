import { Accounts } from 'meteor/accounts-base'
import { Users } from '../../api/users'
import { Meteor } from 'meteor/meteor';


this.handleError = (error) => {
    if (error.error === 'not-authorized') {
      FlashMessages.sendError('You need to sign in!');
    } 
    else if (error === 'org-not-found'){
      FlashMessages.sendError('Contact Telstra-ice-spotify-healthcheck administrator to grant you access');
    }
    else if (error === 'state-not-found'){
      FlashMessages.sendError('Please select the most suitable State!');
    }
    else if (error === 'trend-not-found'){
      FlashMessages.sendError('Please select the most suitable Trend!');
    }
    else if (error === 'state-and-trend-not-found'){
      FlashMessages.sendError('Please select the State and the Trend!');
    }
    else {
      FlashMessages.sendError(error.message);
    }
};

this.showInfo = (message) => {
    FlashMessages.sendInfo(message);
};

Accounts.ui.config({
  requestPermissions: {
    github: ['read:org']
  }
});

Accounts.onLogin(function() {
  // console.log("Logged in!");
  Meteor.call('userData.getOrgAuth', function (error, result)  {
    if(error){
      handleError(error);
    }
  });
});
Accounts.onLogout(function(){
  // console.log("Logging out!");
  Session.clearPersistent();
})
