import { Meteor } from 'meteor/meteor'
import { Checks } from '../../api/checks'
import { Users } from '../../api/users'
import { Mongo } from 'meteor/mongo';
import { Session } from 'meteor/session'

import './home.html'

Template.home.onCreated(() => {
    Meteor.subscribe('checks');
    Meteor.subscribe("userData");

});

Template.home.helpers({
    checks() {
        console.log(Checks.find({ finalized: true , owner: Meteor.userId() }, { sort: { createdAt: -1 }, limit: 15 }).fetch());
        return Checks.find({ finalized: true, owner: Meteor.userId() }, { sort: { createdAt: -1 }, limit: 15 });
    },
    checksInProgress() {
        return Checks.find({ finalized: false, open: true }, { sort: { createdAt: -1 }, limit: 15 });
    },
    checksPublished(checkId) {
        var check = Checks.find({ _id : checkId }).fetch();
        return check[0].published;
    },
    parseIsoDate(isoDate) {
        return moment(isoDate).fromNow();
    },
    getStatsUrl(checkId) {
       return FlowRouter.path('stats', { checkId }); 
    },
    status() {
        return Session.get('org');
    },
    getFinalizeUrl(checkId){
        return FlowRouter.path('finalize', { checkId }); 
    },
    checksSize() {
        var checkSize = Checks.find({ finalized: true }, { sort: { createdAt: -1 }, limit: 15 });
        if (checkSize.count() > 0){
            return true;
        }
        return false;
    },
    checksInProgressSize(){
        var checkInProgressSize = Checks.find({ finalized: false, open: true });
        if (checkInProgressSize.count() > 0){
            return true;
        }
        return false;
    },
    adminStatus() {
        //console.log("Admin status is " + Session.get('admin'));
        if (Session.get('admin') == null){
            return false;
        }
        return Session.get('admin');
    }
});

Template.home.events({
    'click #start-new-sqc'() {
        
        Meteor.call('checks.insert', (error, result) => {
            if (error) {
                handleError(error);
            } else if (result) {
                FlowRouter.go('prepare', { checkId: result });
            }
        });
    },
    'click #published-health-checks'() {
         FlowRouter.go('published');
    }
});
