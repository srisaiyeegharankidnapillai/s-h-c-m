import { Meteor } from 'meteor/meteor';
import { Checks } from '../../api/checks'
import { Mongo } from 'meteor/mongo';
import { Session } from 'meteor/session';

import './published.html'

Template.published.onCreated(() => {

    Meteor.subscribe("publishedChecks");
    Meteor.subscribe("user");
    if (!Session.get('admin')){
                FlowRouter.go('home');
            }
})

Template.published.helpers({
    checksPublished() {
        return Checks.find({ finalized: true , published: true }, { sort: { createdAt: -1 }, limit: 15 });
    },
    checksPublishedCount() {
        var checkSize = Checks.find({ finalized: true , published: true }, { sort: { createdAt: -1 }, limit: 15 });
        if (checkSize.count() == 0){
            return true;
        }
        return false;
    },
    getCheckCreatedMonth(isoDate) {
        return moment(isoDate).format("Do, MMMM");
    },
    getStatsUrl(checkId) {
       return FlowRouter.path('stats', { checkId }); 
    },
    getUserProfileName(userId){
        var user = Meteor.users.find( {_id : userId}).fetch();
        return user[0].profile.name;
    }

});