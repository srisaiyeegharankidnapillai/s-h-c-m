import { CheckCards } from '../../api/checkCards.js'
import { Checks } from '../../api/checks.js'
import { Answers } from '../../api/answers.js'
import { Session } from 'meteor/session'

import './stats.html'

let returnMaxFromResultSet = (resultSet) => {
    let max = {
        _id: null,
        count: null
    };

    if (resultSet) {
        for (var i = 0; i < resultSet.length; i++) {
            if (max.count < resultSet[i].count) {
                max = resultSet[i];
            }
        }
    }

    return max;
};

Template.stats.onCreated(() => {
    const checkId = FlowRouter.getParam('checkId');

    Meteor.subscribe('check', checkId, () => {

        if (!Checks.find({ _id: checkId, owner: Meteor.userId() }).count())
        {
            if (!Session.get('admin')){
                FlowRouter.go('home');
            }
            
        }
    });
    
    Meteor.subscribe('checkCards', checkId);
    Meteor.subscribe('answers', checkId);
});

Template.stats.helpers({
    cards() {
        return CheckCards.find({ checkId: FlowRouter.getParam('checkId'), active: true });
    },
    
    countTrendByValue(cardId, value) {
        return Answers.find({ checkId: FlowRouter.getParam('checkId'), checkCardId: cardId, trend: value }).count();
    },
    
    countStateByValue(cardId, value) {
        return Answers.find({ checkId: FlowRouter.getParam('checkId'), checkCardId: cardId, state: value }).count();
    },
    getStateTooltip(cardId, value){
        var state = Answers.find({ checkId: FlowRouter.getParam('checkId'), checkCardId: cardId, state: value }).count();
        var noMember = "No Squad members have voted for this";
        if (value == 1) {
            if (state == 0){
                return noMember;
            }
            else if (state == 1){
                 return state + " member of the squad is happy with this, and see no major need for improvement right now";
            }
            else 
            {
                return state + " members of the squad are happy with this, and see no major need for improvement right now";
            }
        }
        if (value == 0) {
            if (state == 0){
                return noMember;
            }
            else if (state == 1){
                 return state + " member of the squad thinks there are some important problems that need addressing, but it’s not a disaster";
            }
            else 
            {
                return state + " members of the squad think there are some important problems that need addressing, but it’s not a disaster";
            }
        }
        if (value == -1) {
            if (state == 0){
                return noMember;
            }
            else if (state == 1){
                 return state + " member of the squad thinks that this needs to be improved";
            }
            else 
            {
                return state + " members of the squad think that this needs to be improved";
            }
        }
    },
    getTrendTooltip(cardId, value){
        var trend =  Answers.find({ checkId: FlowRouter.getParam('checkId'), checkCardId: cardId, trend: value }).count();
        if (value == 1) {
            if (trend == 0){
                return "No Squad members have voted for this";
            }
            else if (trend == 1){
                 return trend + " member of the squad feels that this is going to improve";
            }
            else 
            {
                return trend + " members of the squad feel that this is going to improve";
            }
        }
        if (value == 0) {
            if (trend == 0){
                return "No Squad members have voted for this";
            }
            else if (trend == 1){
                 return trend + " member of the squad feels that this is going to remain the same";
            }
            else 
            {
                return trend + " members of the squad feel that this is going to remain the same";
            }
        }
        if (value == -1) {
            if (trend == 0){
                return "No Squad members have voted for this";
            }
            else if (trend == 1){
                 return trend + " member of the squad feels that this is going to get worse";
            }
            else 
            {
                return trend + " members of the squad feel that this is going to get worse";
            }
        } 
    },
    getOverallIconClass(cardId) {
        const states = ReactiveMethod.call('getAnswersStateData', FlowRouter.getParam('checkId'), cardId);
        const trends = ReactiveMethod.call('getAnswersTrendData', FlowRouter.getParam('checkId'), cardId);

        let state = returnMaxFromResultSet(states);
        let trend = returnMaxFromResultSet(trends);

        return 'overall-' + state._id + '-' + trend._id;
    },
    getCheck(){
        return Checks.find({ _id: FlowRouter.getParam('checkId')});
    },
    getCurrentCardOwner(){
        var check =  Checks.find({ _id : FlowRouter.getParam('checkId') }).fetch();
        var owner = check[0].owner;
        if (owner == Meteor.userId()){
            return true;
        }
        else 
        {
            return false;
        }
    }

});

Template.stats.events({
    'click #publish'() {
        event.preventDefault();
        Meteor.call('checks.updatePublish', FlowRouter.getParam('checkId'), true , (error) => {
            if (error) {
                handleError(error);
            } else {
                showInfo('Your Squad Health Check has been Published!')
            }
        });
    },
    'click #unpublish'() {
        event.preventDefault();
        Meteor.call('checks.updatePublish', FlowRouter.getParam('checkId'), false , (error) => {
            if (error) {
                handleError(error);
            } else {
                showInfo('Your Squad Health Check has been Unpublished!')
            }
        });
    }
});
